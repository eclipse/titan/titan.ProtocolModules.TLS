///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
//                                                                           
///////////////////////////////////////////////////////////////////////////////
//
//  File:               TLS_Constants.ttcn
//  Description:        TLS Constants
//  Rev:                R4A
//  Prodnr:             CNL 113 806
//  Updated:            2014-06-13
//  Contact:            http://ttcn.ericsson.se
//  Reference:          http://tools.ietf.org/html/rfc4346
//
//


module TLS_Constants {
    import from TLS_Types all;
    
    const ProtocolVersion cg_TLSv1_1_PROTOCOL_VERSION           := { 3, 2 };

    const CipherSuite cg_TLS_NULL_WITH_NULL_NULL                := { '00'O, '00'O };
    const CipherSuite cg_TLS_RSA_WITH_NULL_MD5                  := { '00'O, '01'O };
    const CipherSuite cg_TLS_RSA_WITH_NULL_SHA                  := { '00'O, '02'O };
    const CipherSuite cg_TLS_RSA_WITH_RC4_128_MD5               := { '00'O, '04'O };
    const CipherSuite cg_TLS_RSA_WITH_RC4_128_SHA               := { '00'O, '05'O };
    const CipherSuite cg_TLS_RSA_WITH_IDEA_CBC_SHA              := { '00'O, '07'O };
    const CipherSuite cg_TLS_RSA_WITH_DES_CBC_SHA               := { '00'O, '09'O };
    const CipherSuite cg_TLS_RSA_WITH_3DES_EDE_CBC_SHA          := { '00'O, '0A'O };
    
    const CipherSuite cg_TLS_DH_DSS_WITH_DES_CBC_SHA            := {'00'O, '0C'O };
    const CipherSuite cg_TLS_DH_DSS_WITH_3DES_EDE_CBC_SHA       := {'00'O, '0D'O };
    const CipherSuite cg_TLS_DH_RSA_WITH_DES_CBC_SHA            := {'00'O, '0F'O };
    const CipherSuite cg_TLS_DH_RSA_WITH_3DES_EDE_CBC_SHA       := {'00'O, '10'O };
    const CipherSuite cg_TLS_DHE_DSS_WITH_DES_CBC_SHA           := {'00'O, '12'O };
    const CipherSuite cg_TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA      := {'00'O, '13'O };
    const CipherSuite cg_TLS_DHE_RSA_WITH_DES_CBC_SHA           := {'00'O, '15'O };
    const CipherSuite cg_TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA      := {'00'O, '16'O };
    
    const CipherSuite cg_TLS_DH_anon_WITH_RC4_128_MD5           := { '00'O, '18'O };
    const CipherSuite cg_TLS_DH_anon_WITH_DES_CBC_SHA           := { '00'O, '1A'O };
    const CipherSuite cg_TLS_DH_anon_WITH_3DES_EDE_CBC_SHA      := { '00'O, '1B'O };
    
    const CipherSuite cg_TLS_RSA_EXPORT_WITH_RC4_40_MD5         := { '00'O, '03'O };
    const CipherSuite cg_TLS_RSA_EXPORT_WITH_RC2_CBC_40_MD5     := { '00'O, '06'O };
    const CipherSuite cg_TLS_RSA_EXPORT_WITH_DES40_CBC_SHA      := { '00'O, '08'O };
    const CipherSuite cg_TLS_DH_DSS_EXPORT_WITH_DES40_CBC_SHA   := { '00'O, '0B'O };
    const CipherSuite cg_TLS_DH_RSA_EXPORT_WITH_DES40_CBC_SHA   := { '00'O, '0E'O };
    const CipherSuite cg_TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA  := { '00'O, '11'O };
    const CipherSuite cg_TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA  := { '00'O, '14'O };
    const CipherSuite cg_TLS_DH_anon_EXPORT_WITH_RC4_40_MD5     := { '00'O, '17'O };
    const CipherSuite cg_TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA  := { '00'O, '19'O };
    
    const CipherSuite cg_TLS_KRB5_WITH_DES_CBC_SHA              := { '00'O, '1E'O };
    const CipherSuite cg_TLS_KRB5_WITH_3DES_EDE_CBC_SHA         := { '00'O, '1F'O };
    const CipherSuite cg_TLS_KRB5_WITH_RC4_128_SHA              := { '00'O, '20'O };
    const CipherSuite cg_TLS_KRB5_WITH_IDEA_CBC_SHA             := { '00'O, '21'O };
    const CipherSuite cg_TLS_KRB5_WITH_DES_CBC_MD5              := { '00'O, '22'O };
    const CipherSuite cg_TLS_KRB5_WITH_3DES_EDE_CBC_MD5         := { '00'O, '23'O };
    const CipherSuite cg_TLS_KRB5_WITH_RC4_128_MD5              := { '00'O, '24'O };
    const CipherSuite cg_TLS_KRB5_WITH_IDEA_CBC_MD5             := { '00'O, '25'O };
    
    const CipherSuite cg_TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA    := { '00'O, '26'O};
    const CipherSuite cg_TLS_KRB5_EXPORT_WITH_RC2_CBC_40_SHA    := { '00'O, '27'O};
    const CipherSuite cg_TLS_KRB5_EXPORT_WITH_RC4_40_SHA        := { '00'O, '28'O};
    const CipherSuite cg_TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5    := { '00'O, '29'O};
    const CipherSuite cg_TLS_KRB5_EXPORT_WITH_RC2_CBC_40_MD5    := { '00'O, '2A'O};
    const CipherSuite cg_TLS_KRB5_EXPORT_WITH_RC4_40_MD5        := { '00'O, '2B'O};
    
    const CipherSuite cg_TLS_RSA_WITH_AES_128_CBC_SHA           := { '00'O, '2F'O };
    const CipherSuite cg_TLS_DH_DSS_WITH_AES_128_CBC_SHA        := { '00'O, '30'O };
    const CipherSuite cg_TLS_DH_RSA_WITH_AES_128_CBC_SHA        := { '00'O, '31'O };
    const CipherSuite cg_TLS_DHE_DSS_WITH_AES_128_CBC_SHA       := { '00'O, '32'O };
    const CipherSuite cg_TLS_DHE_RSA_WITH_AES_128_CBC_SHA       := { '00'O, '33'O };
    const CipherSuite cg_TLS_DH_anon_WITH_AES_128_CBC_SHA       := { '00'O, '34'O };
    
    const CipherSuite cg_TLS_RSA_WITH_AES_256_CBC_SHA           := { '00'O, '35'O };
    const CipherSuite cg_TLS_DH_DSS_WITH_AES_256_CBC_SHA        := { '00'O, '36'O };
    const CipherSuite cg_TLS_DH_RSA_WITH_AES_256_CBC_SHA        := { '00'O, '37'O };
    const CipherSuite cg_TLS_DHE_DSS_WITH_AES_256_CBC_SHA       := { '00'O, '38'O };
    const CipherSuite cg_TLS_DHE_RSA_WITH_AES_256_CBC_SHA       := { '00'O, '39'O };
    const CipherSuite cg_TLS_DH_anon_WITH_AES_256_CBC_SHA       := { '00'O, '3A'O };
    
} with { encode "RAW" } //end of module
